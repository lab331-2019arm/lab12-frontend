import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentService } from './service/student-service';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StudentsFileImplService } from './service/students-file-impl.service';
import { StudentsComponent } from './students/list/students.component';
import { StudentsAddComponent } from './students/add/students.add.component';
import { StudentsViewComponent } from './students/view/students.view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule
  , MatIconModule, MatListModule, MatGridListModule, MatCardModule
  , MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, MatProgressSpinnerModule, MatSelectModule, MatRadioModule
} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { StudentRoutingModule } from './students/student-routing.module';
import { StudentTableComponent } from './students/student-table/student-table.component';
import { StudentRestImplService } from  './service/student-rest-impl.service';
import { MatFileUploadModule } from 'mat-file-upload';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { CourseTableComponent } from './course/course-table/course-table.component';
import { CourseRoutingModule } from './course/course-routing.module';
import { CourseServiceService } from './service/course-service.service';
import { CourseRestImplService } from './service/course-rest-impl.service';
import { CourseAddComponent } from './course/course-add/course-add.component';
import { LecturerServiceService } from './service/lecturer-service.service';
import { LecturerRestImplService } from './service/lecturer-rest-impl.service';
import { CourseInfoComponent } from './course/course-info/course-info.component';

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentsAddComponent,
    StudentsViewComponent,
    MyNavComponent,
    FileNotFoundComponent,
    StudentTableComponent,
    CourseTableComponent,
    CourseAddComponent,
    CourseInfoComponent,
  
  ],
  imports: [
    BrowserModule,
    MatSelectModule,
    MatRadioModule,
    MatFileUploadModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    StudentRoutingModule,
    CourseRoutingModule,
    AppRoutingModule,
    ReactiveFormsModule,
    
  ],
  providers: [
    { provide: StudentService, useClass: StudentRestImplService  },
    { provide: CourseServiceService, useClass: CourseRestImplService},
    { provide: LecturerServiceService, useClass: LecturerRestImplService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
